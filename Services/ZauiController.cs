﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using primaltribe.spaBanff.Components;
using primaltribe.spaBanff.Services.ViewModels;
using DotNetNuke.Common;
using DotNetNuke.Web.Api;
using DotNetNuke.Security;
using System.Threading;
using DotNetNuke.UI.Modules;
using DotNetNuke.Common.Utilities;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;

using System.Text.RegularExpressions;

namespace primaltribe.spaBanff.Services
{
	[SupportedModules("Banff")]
	[DnnModuleAuthorize(AccessLevel = SecurityAccessLevel.View)]

	public class ZauiController : DnnApiController
	{
		private readonly IItemRepository _repository;

		public ZauiController(IItemRepository repository)
		{
			Requires.NotNull(repository);

			this._repository = repository;
		}

		public ZauiController() : this(ItemRepository.Instance) { }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public HttpResponseMessage Ping()
		{
			string responseText = "";
			WebRequest req = null;
			WebResponse rsp = null;
			try
			{
				string xml = "<request><zapiToken>6fee51bf123251fc9c58564959d8a028ce7e62ca</zapiToken><zapiAccountId>623</zapiAccountId><zapiUserId>126188</zapiUserId><zapiMethod><methodName>zapiPing</methodName></zapiMethod></request>";
				string uri = "https://banffadventures.zaui.net/zapi/";
				req = WebRequest.Create(uri);
				//req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy
				req.Method = "POST";        // Post method
				req.ContentType = "text/xml";     // content type
												  // Wrap the request stream with a text-based writer
				StreamWriter writer = new StreamWriter(req.GetRequestStream());
				// Write the XML text into the stream
				writer.WriteLine(xml);
				writer.Close();
				// Send the data to the webserver
				rsp = req.GetResponse();
				var encoding = ASCIIEncoding.ASCII;
				using (var reader = new System.IO.StreamReader(rsp.GetResponseStream(), encoding))
				{
					responseText = reader.ReadToEnd();
				}
				//Convert xml to json
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(responseText);
				XmlNodeList res = doc.GetElementsByTagName("response");
				responseText = Newtonsoft.Json.JsonConvert.SerializeXmlNode(res[0]);
			}
			catch (WebException webEx)
			{

			}
			catch (Exception ex)
			{

			}
			finally
			{
				if (req != null) req.GetRequestStream().Close();
				if (rsp != null) rsp.GetResponseStream().Close();
			}

			return Request.CreateResponse(System.Net.HttpStatusCode.OK, responseText);
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public HttpResponseMessage getZauiData(ZauiDTO methodDTO)
		{
			string responseText = "";
			WebRequest req = null;
			WebResponse rsp = null;
			try
			{
				string xml = "<request><zapiToken>6fee51bf123251fc9c58564959d8a028ce7e62ca</zapiToken><zapiAccountId>623</zapiAccountId><zapiUserId>126188</zapiUserId><zapiMethod><methodName>" + methodDTO.methodName + "</methodName></zapiMethod></request>";
				string uri = "https://banffadventures.zaui.net/zapi/";
				req = WebRequest.Create(uri);
				//req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy
				req.Method = "POST";        // Post method
				req.ContentType = "text/xml";     // content type
												  // Wrap the request stream with a text-based writer
				StreamWriter writer = new StreamWriter(req.GetRequestStream());
				// Write the XML text into the stream
				writer.WriteLine(xml);
				writer.Close();
				// Send the data to the webserver
				rsp = req.GetResponse();
				var encoding = ASCIIEncoding.ASCII;
				using (var reader = new System.IO.StreamReader(rsp.GetResponseStream(), encoding))
				{
					responseText = reader.ReadToEnd();
				}

				//Convert xml to json
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(responseText);
				
				XmlNodeList res = doc.GetElementsByTagName("response");
				responseText = Newtonsoft.Json.JsonConvert.SerializeXmlNode(res[0]);
				
			}
			catch (WebException webEx)
			{

			}
			catch (Exception ex)
			{

			}
			finally
			{
				if (req != null) req.GetRequestStream().Close();
				if (rsp != null) rsp.GetResponseStream().Close();
			}

			return Request.CreateResponse(System.Net.HttpStatusCode.OK, responseText);
		}

	}

	public class ZauiDTO
	{
		public string methodName { get; set; }
	}

}
